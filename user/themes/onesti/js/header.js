const navBar = $('.nav-bar')
const indicator = $('#menu-item-indicator')
const mobileIndicator = $('#mobile-menu-indicator')
const menuItems = $('.nav-bar-menu-item')
const navBarTitle = $('#nav-bar-title')
const indicatorOffset = parseFloat(indicator.offset().left.toFixed(2))
const indicatorWidth = indicator.width()
const menuMobile = $('#nav-bar-mobile-menu')
const navBarMobileLogo = $('#nav-bar-mobile-logo')
const menuMobileButton = $('#menu-mobile-btn')
let mobileIndicatorOffset = parseFloat((mobileIndicator.offset().top - $(window).scrollTop()).toFixed(2))
const mobileIndicatorHeight = 24

const sectionsArr = [
  $('#unicos'),
  $('#viemos'),
  $('#estivemos'),
  $('#vemos'),
  $('#pensamos'),
  $('#vivemos'),
  $('#praticamos'),
  $('#temos'),
  $('#atender'),
  $('#solucoes'),
  $('#atuacao'),
  $('#depoimentos'),
  $('#cases')
]

menuItems.mouseenter(function () {
  const positionToGo = parseFloat($(this).offset().left.toFixed(2))
  const widthToExpand = parseFloat($(this).children(":first").width().toFixed(2))
  indicator.css('transform', `
    translateX(${positionToGo - indicatorOffset}px)
    scaleX(${widthToExpand / indicatorWidth})
  `)
})

navBar.mouseleave(function () {
  indicator.css('transform', 'translateX(0) scaleX(0)')
})
const firstSection = $('#bannerTop')
let currentMenuTitle
$(window).scroll(function () {
  navBar.toggleClass('nav-bar--black', !firstSection.visible(true))
  sectionsArr.every((section, index) => {
    const sectionTop = section.offset().top
    const windowScrollTop = $(window).scrollTop()
    const sectionPosition = sectionTop - windowScrollTop
    if (sectionPosition < 0) {
      return true
    } else if (sectionPosition <= 80) {
      currentMenuTitle = section[0].getAttribute('data-title')
      return false
    }
    else {
      if (index === 0) {
        currentMenuTitle = 'Menu'
        return false
      }
    }
  })
  navBarTitle.text(currentMenuTitle)
})

$('.menu-link').click(function () {
  menuMobile.removeClass('nav-bar-mobile-menu--visible')
  menuMobileButton.removeClass('nav-bar-mobile__menu-btn--close')
  menuMobileButton.children('i').toggleClass('fa-bars')
  menuMobileButton.children('i').toggleClass('fa-times')
  navBarMobileLogo.removeClass('nav-bar-mobile__logo--large')
  const sectionIdToScroll = '#' + $(this)[0].getAttribute('data-to')
  TweenLite.to(window, 0.8, { scrollTo: sectionIdToScroll, ease: Power3.easeInOut })
})

let selectedOptionTop
$('.mobile-menu-item').click(function () {
  const subMenu = $(this).find('ul')
  if (subMenu.hasClass('mobile-menu-item__submenu--visible')) {
    subMenu.removeClass('mobile-menu-item__submenu--visible')
  } else {
    $('.mobile-menu-item__submenu--visible').removeClass('mobile-menu-item__submenu--visible')
    subMenu.toggleClass('mobile-menu-item__submenu--visible')
  }
  setTimeout(() => {
    selectedOptionTop = parseFloat(($(this).offset().top - $(window).scrollTop()).toFixed(2))
    mobileIndicator.css('transform', `translateY(${selectedOptionTop - mobileIndicatorOffset + (mobileIndicatorHeight / 2)}px)`)
  }, 500)
})

menuMobileButton.click(function () {
  menuMobile.toggleClass('nav-bar-mobile-menu--visible')
  navBarMobileLogo.toggleClass('nav-bar-mobile__logo--large')
  menuMobileButton.toggleClass('nav-bar-mobile__menu-btn--close')
  menuMobileButton.children('i').toggleClass('fa-bars')
  menuMobileButton.children('i').toggleClass('fa-times')
})

console.log(mobileIndicatorOffset)
$(window).resize(function () {
  const mobileMenuFirstOptionTop = $('.nav-bar-mobile-menu__drawer .mobile-menu-item:first-child').offset().top
  mobileIndicatorOffset = parseFloat((mobileMenuFirstOptionTop - $(window).scrollTop() + 8).toFixed(2))
  console.log(mobileIndicatorOffset)
  // console.log(mobileIndicatorOffset)
  // if (selectedOptionTop) {
  //   mobileIndicator.css('transform', `translateY(${selectedOptionTop - mobileIndicatorOffset + (mobileIndicatorHeight / 2)}px)`)
  // }
})
