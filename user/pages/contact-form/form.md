---
title: A Page with an Example Form
form:
    name: contact-form
    fields:
        - name: nome
          placeholder: nome
          autofocus: on
          autocomplete: on
          type: text
          validate:
            required: false

        - name: e-mail
          label: e-mail
          placeholder: e-mail
          type: email
          validate:
            required: true
        
        - name: telefone
          label: telefone
          placeholder: telefone
          type: text
          validate:
            required: false
        
        - name: assunto
          label: assunto
          placeholder: assunto
          type: text
          validate:
            required: false

        - name: mensagem
          label: mensagem
          # placeholder: telefone
          type: textarea
          validate:
            required: false

    buttons:
        - type: submit
          value: ENVIAR
        # - type: reset
        #   value: Reset

    process:
        - email:
            from: "{{ config.plugins.email.from }}"
            to:
              - "{{ config.plugins.email.to }}"
              - "{{ form.value.email }}"
            subject: "[Feedback] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
            body: "{% include 'forms/data.txt.twig' %}"
        - message: Thank you for your feedback!
        - display: thankyou
---