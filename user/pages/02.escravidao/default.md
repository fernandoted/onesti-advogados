---
title: Escravidao
---

<section id="banner-slave">
    <h1>
        DECLARAÇÃO SOBRE ESCRAVIDÃO MODERNA
    </h1>
    <div class="arrowDown-wrapper">
        <a href="#second-screen"><i class="fas fa-angle-down"></i></a>
    </div>
    {# <div class="backTop-wrapper"> #}
    <button onclick="topFunction()" id="backTop" title="go to top"><i class="fas fa-arrow-up"></i></button>
    {# </div> #}
</section>
<section class="container grid-lg" id="intro-slave">
    <div class="titleContainer">
        <div class="titleSection-wrapper">
            <span class="squareTitle">&#9632;</span>
            <h3 class="language" lang="pt-br">INTRODUÇÃO</h3>
        </div>
        <hr class="subTitleLine">
		<p class="titleCompl language" lang="pt-br">VALORIZAMOS O SER HUMANO</p>
        <div class="block-cols">
            <p class="col-half">Nós estamos alinhados com a Declaração Universal dos Direitos Humanos, os Princípios de Direitos Humanos do Pacto Global da ONU, os Princípios de Empoderamento das Mulheres, as Diretrizes da OCDE e os Princípios Orientadores das Nações Unidas sobre Empresas e Direitos Humanos. Nós nos posicionamos contra a escravidão e o tráfico humano.</p>
            <div class="col-half">
                <h2>juntos<br>somos +<br>fortes!</h2>
            </div>
        </div>
    </div>
</section>
<section class="container grid-lg" id="negocio-slave">
    <div class="titleContainer">
        <div class="titleSection-wrapper">
            <span class="squareTitle">&#9632;</span>
            <h3 class="language" lang="pt-br">NOSSO <span>NEGÓCIO</span></h3>
        </div>
        <hr class="subTitleLine">
        <p class="titleCompl language" lang="pt-br">MAIS DO QUE VER, VIVER A EXPERIÊNCIA</p>
    </div>
    <div>
        <p>Somos uma sociedade de advogados, inscrita na Ordem dos Advogados do Brasil, Secção de São Paulo. Atuamos em todo Brasil e em alguns países do mundo. Nossa cultura é baseada na filosofia de evolução contínua de nossos valores e principios.</p>
        <p>Consideramos muito baixa a possibilidade de possuir mão de obra escrava dentro de nosso negócio ou de nossa cadeia de fornecedores, mas temos consciência que trata-se de um problema que ainda assola a sociedade moderna, ocorrendo de diversas formas em todo o mundo, inclusive no Brasil.</p>
    </div>
</section>
<div class="black-belt"> <!-- gray belt -->
    <p><span>onesti</span>advogados<span>.com.br</span>
</div>
<div class="gray-belt"></div> <!-- black belt -->
<div class="red-belt"></div> <!-- red belt -->
<section class="container grid-lg" id="politica-slave">
    <div class="titleContainer">
        <div class="titleSection-wrapper">
            <span class="squareTitle">&#9632;</span>
            <h3 class="language" lang="pt-br">NOSSA <span>POLÍTICA</span></h3>
        </div>
        <hr class="subTitleLine">
        <p class="titleCompl language" lang="pt-br">MAIS DO QUE VER, VIVER A EXPERIÊNCIA</p>
    </div>
    <div class="block-cols">
        <div class="col-half">
            <div class="image-wrapper">
                <img src="{{ url('theme://images/slave-pic-01.jpg') }}">
            </div>
        </div>
        <div class="col-half">
            <p>Defender, proteger e fortalecer o Estado de Direito, para que todos possam se beneficiar da justiça, paz e certeza que ele traz deve ser nossa maior contribuição para um futuro próspero e sustentável. Isso deve refletir o nosso propósito: de proporcionar a segurança jurídica em um mundo em constante transformação. Executamos nossas operações com  responsabilidade. Estamos comprometidos em garantir que a escravidão, o tráfico de pessoas, a exploração do ser humano, o trabalho infantil e qualquer outro abuso de direitos humanos não tenham lugar em nosso negócio, seja direta ou indiretamente. Reforçamos este compromisso com essa declaração.<p>
        </div>
    </div>
</section>

<section class="container grid-lg" id="relac-slave">
    <div class="titleContainer">
        <div class="titleSection-wrapper">
            <span class="squareTitle">&#9632;</span>
            <h3 class="language" lang="pt-br">NOSSOS <span>RELACIONAMENTOS</span></h3>
        </div>
        <hr class="subTitleLine">
        <p class="titleCompl language" lang="pt-br">BUSCAMOS NOSSOS IGUAIS</p>
    </div>
    <div class="block-cols">
        <p>Ao contratar o fornecimento de produtos ou serviços realizamos avaliações considerando alguns fatores, dentre os quais valem citar: (1) local onde o serviço ou bens são fornecidos, (2) o setor industrial a partir do qual os serviços ou bens são fornecidos, e (3) as práticas trabalhistas específicas para a indústria e a localização a partir da qual os serviços ou bens específicos são fornecidos.</p>
        <p>As práticas trabalhistas incluem a consideração de questões relacionadas ao recrutamento durante o emprego e rescisão do contrato de trabalho. Esperamos que todos aqueles que fazem negócios conosco, incluindo os de nossa cadeia de suprimentos, tenham o mesmo compromisso, como nós, com a nossa Política. Nosso foco tem sido a conscientização sobre as obrigações legais e sociais necessários para o crescimento das comunidades que atuamos.</p>
    </div>
</section>

<section class="container grid-lg" id="compromisso-slave">
    <div class="titleContainer">
        <div class="titleSection-wrapper">
            <span class="squareTitle">&#9632;</span>
            <h3 class="language" lang="pt-br">NOSSO <span>COMPROMISSO</span></h3>
        </div>
        <hr class="subTitleLine">
        <p class="titleCompl language" lang="pt-br">BUSCAMOS UM MUNDO MELHOR</p>
    </div>
    <div class="block-cols">
        <p>Em conjunto com nossas políticas complementares e manuais de conduta do escritório, esta declaração faz parte das medidas que tomamos para garantir que a escravidão e o tráfico de seres humanos não estejam ocorrendo em nossos negócios ou em nossa cadeia de fornecimento. Isto reforça nosso compromisso de ser uma empresa visivelmente responsável, ao mesmo tempo que aborda as nossas próprias obrigações ao em prol de uma sociedade mais justa, responsável e sustentável.</p>
    </div>
    <div class="avatar-bottom-row">
        <div class="avatar-wrapper">
            <img src="{{ url('theme://images/onesti-avatar-gray.png') }}">
        </div>
    </div>
</section>

<footer class="footer-pages">
    <div class="container grid-lg">
        <small>Essa política foi aprovada pela Diretoria de Onesti Sociedade de Advogados, OAB/SP 27.583, inscrita no CNPJ/MF nº 31.431.327/0001-41</small>
    </div>
</footer>
